import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as plticker
import numpy as np
import datetime

class Graph(object):
	def __init__(self, match_data, inventory_value=None):
		self.inventory_value = inventory_value
		input_data = match_data
		self.convert_dates(input_data)
		parsed_data = self.parse_duplicates(input_data)
		self.calculate_values(parsed_data)
		self.dates = [x[0] for x in parsed_data]
		self.values = [x[1] for x in parsed_data]
	def convert_dates(self, data):
		for match in data:
			date = datetime.datetime.strptime(match[0], "%Y-%m-%d %H:%M:%S")
			new_date = datetime.datetime(date.year, date.month, date.day, 0,0,0,0)
			match[0] = new_date
	def parse_duplicates(self, data):
		dates = []
		#Find unique dates
		for x in data:
			if x[0] not in dates:
				dates.append(x[0])
		new_data = []
		#Sum all the values that have the same date
		for date in dates:
			total_value = 0
			for match in data:
				if match[0] == date:
					total_value += match[1]
			new_data.append([date, total_value])
		return new_data
	#Calculate total value earned for each date
	def calculate_values(self, data):
		total_value = 0
		for match in data:
			total_value += match[1]
			match[1] = total_value
	def create_graph(self):
		font = {
			'family' : 'serif',
	        'color'  : 'darkred',
	        'weight' : 'normal',
	        'size'   : 17,
        }
		dates = self.dates
		x = dates
		y = self.values
		locator = mdates.AutoDateLocator()
		rect = plt.figure().patch
		rect.set_facecolor("#FFFFFF")
		plt.gca().xaxis.set_major_formatter(mdates.AutoDateFormatter(locator))
		plt.gca().xaxis.set_minor_formatter(mdates.DateFormatter("%d"))
		plt.gca().xaxis.set_major_locator(locator)
		plt.gca().autoscale_view()
		plt.gcf().canvas.set_window_title('Graph')
		plt.plot(x, y, "ro-")
		plt.gcf().autofmt_xdate()
		if not self.inventory_value == None:
			plt.title("Value earned over time\nCurrent lounge inventory value: $" + str(self.inventory_value), font)
		else:
			plt.title("Value earned over time")
		plt.ylabel("Value Earned ($)", font)
		plt.show()
