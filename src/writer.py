import json
import io
import os
import codecs

def read_items(path):
	items = {}
	if os.path.isfile(path + "items.txt"):
		with io.open(path + "items.txt", "r", encoding="utf-8-sig") as text:
			data = text.read()
			split_data = data.split("\n") 
			for item in split_data:
				properties = item.split("=")
				if len(properties) == 2:
					items.update({unicode(properties[0]):float(properties[1])})
	return items
			
def write_items(items, path):
	text = io.open(path + "items.txt", "w", encoding="utf-8-sig")
	for key, value in items.iteritems():
		text.write(key + "=" + unicode(value) + "\n")
	text.close()
def read_matches(path):
	matches = []
	if os.path.isfile(path + "matches.txt"):
		with open(path + "matches.txt", "r") as text:
			data = text.read()
			split_data = data.split("\n")
			for match in split_data:
				properties = match.split("=")
				if len(properties) == 2:
					matches.append([properties[0], float(properties[1])])
	return matches
def write_matches(matches, path):
	with open(path + "matches.txt", "w") as text:
		for match in matches:
			text.write(match[0] + "=" + str(match[1]) + "\n")
def read_cookie(path):
	cookie_path = path + "cookie.txt"
	if os.path.isfile(cookie_path) and not os.stat(cookie_path)[6]==0:
		with open(cookie_path, "r") as text:
			return text.read().strip()
	else:
		return None