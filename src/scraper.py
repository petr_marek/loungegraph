# -*- coding: utf-8 -*-
import datetime as dt
import urllib2, urllib
import time
import gzip
import StringIO
import sys
import converter as conv
from threading import Thread
from bs4 import BeautifulSoup
class InvalidCookie(Exception):
	def __init__(self, message):
		self.message = message
	def __str__(self):
		return self.message
class InventoryValue(object):
	def __init__(self, cookie):
		self.lounge_cookie = cookie
	def get_page(self, cookie):
		url = "http://csgolounge.com/mybets"
		headers = {
				"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
				"Accept-Encoding": "gzip,deflate,sdch",
				"Accept-Language": "q=0.8,en-US;q=0.6,en;q=0.4",
				"Cache-Control": "max-age=1",
				"Connection": "keep-alive",
				"Cookie": cookie,
				"Host": "csgolounge.com",
				"Referer": "http://csgolounge.com/mybets",
				"User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36"
		}
		data = {}
		req = urllib2.Request(url, urllib.urlencode(data), headers)
		f = urllib2.urlopen(req)
		response = f.read()
		f.close()
		html = self.decompress_data(response).decode("UTF-8")
		soup = BeautifulSoup(html)
		return soup
 	#Sum up all items in lounge inventory
	def calculate_values(self, html):
		items = html.find_all("div", "item")
		value = 0
		for item in items:
			value += float(item.find("div", "value").get_text()[2:])
		return value
	def get_inventory_value(self):
		soup = self.get_page(self.lounge_cookie)
		return self.calculate_values(soup)
	def decompress_data(self, data):
		gzipper = gzip.GzipFile(fileobj=StringIO.StringIO(data))
		return gzipper.read()

class MatchHistory(object):
	saved_prices = {}
	def __init__(self, cookie, loaded_items, loaded_matches, currency_converter):
		self.saved_prices = loaded_items
		self.saved_matches = loaded_matches
		self.lounge_cookie = cookie
		self.converter = currency_converter
	def get_price(self, item_name, items):
		#Modify item name so that it fits the url format
		clean_name = item_name.replace(' ', '%20').replace('|', '%7C').replace('(', '%28').replace(')', '%29')
		url = "http://steamcommunity.com/market/listings/730/" + clean_name
		headers = {
				"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
				"Accept-Encoding": "gzip,deflate,sdch",
				"Accept-Language": "en-US,en;q=0.8",
				"Connection": "keep-alive",
				"Cookie": "recentlyVisitedAppHubs=730; strInventoryLastContext=730_2; sessionid=MzAwMTc1Njky; Steam_Language=english; steamCC_23_29_124_234=US; timezoneOffset=10800,0; __utma=268881843.938767715.1400925164.1400946752.1400951634.3; __utmb=268881843.0.10.1400951634; __utmc=268881843; __utmz=268881843.1400925164.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)",
				"Host": "steamcommunity.com",
				"User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36"
		}
		data = {}
		req = urllib2.Request(url.encode('UTF-8'), urllib.urlencode(data), headers)
		f = urllib2.urlopen(req)
		response = f.read()

		html = self.decompress_data(response).decode("UTF-8")
		soup = BeautifulSoup(html)
		prices = soup.findAll("span", "market_listing_price_with_fee")
		price = prices[0].get_text().replace(',', '.').replace('-', '0').strip()
		#Find a price that isn't Sold (Steam sometimes doesn't update price in time.)
		for x in prices:
			if not "Sold" in x:
				price = x.get_text().replace(',', '.').replace('-', '0').strip()
			else:
				print "Got \"Sold\" for " + item_name.replace(u"\u2122", "") #Debug

		display_price = "null" 
		#Convert prices since Steam likes to randomly choose currencies
		if u"\u20AC" in price: 
			price = float(price[:-1])
			display_price = str(price) + " Euro"
			price = self.converter.convert_price(price, "euro", "dollar")		
		elif "R" in price:
			price = float(price[3::])
			display_price = str(price) + " BRL"
			price = self.converter.convert_price(price, "brl", "dollar")
		elif "p" in price:
			price = float(price[:-4])
			display_price = str(price) + " Ruble"
			price = self.converter.convert_price(price, "ruble", "dollar")
		elif "USD" in price:
			price = float(price[1:-4])
			display_price = str(price) + " Dollar"
		elif "$" in price:
			price = float(price[1::])
			display_price = str(price) + " Dollar"
		elif u"\xA3" in price:
			price = float(price[1::])
			display_price = str(price) + " Pound"
			price = self.converter.convert_price(price, "pound", "dollar")	
		else:
			print "Unknown price! Input: " + price
			return
		price = float("%.2f" % price) #Round the price
		print item_name.replace(u"\u2122", "") + " | " + display_price + " -> $" + str(price)
		self.saved_prices.update({item_name:price})
		items.append(price)

	def get_match_history(self):
		print "Scraping CS:GO Lounge..."
		url = "http://csgolounge.com/ajax/betHistory.php"
		headers = {
				"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
				"Accept-Encoding": "gzip,deflate,sdch",
				"Accept-Language": "q=0.8,en-US;q=0.6,en;q=0.4",
				"Cache-Control": "max-age=0",
				"Connection": "keep-alive",
				"Cookie": self.lounge_cookie,
				"Host": "csgolounge.com",
				"Referer": "http://csgolounge.com/mybets",
				"User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36"
		}
		data = {}
		req = urllib2.Request(url, urllib.urlencode(data), headers)
		f = urllib2.urlopen(req)
		response = f.read()
		decompressed = self.decompress_data(response)
		return BeautifulSoup(decompressed)
	def get_parsed_matches(self, soup):
		matches = self.get_matches(soup)
		return self.parse_matches(matches)
	def get_matches(self, html=None):
		print "Parsing your bet history..."
		soup = self.get_match_history() if html == None else BeautifulSoup(open(html))
		rows = soup.find_all("tr")[::-1]
		scraped_matches = []
		#Construct matches (Still in html form)
		for x in range(0, len(rows), 3):
			match = [rows[x], rows[x+1], rows[x+2]]
			scraped_matches.append(match)
		if len(scraped_matches) == 0:
			raise InvalidCookie("Invalid cookie or no betting history.")
		new_matches = self.parse_new_matches(scraped_matches)
		print "New matches to parse: " + str(len(new_matches))
		parsed_matches = self.parse_matches(new_matches)
		return self.saved_matches + parsed_matches
	"""
	Parse items from the html and gets prices for them
	Match=[ 
			<tr class="details.."> [0] Won items
			<tr class="details.."> [1] Placed items
			<tr>                   [2] Match status and date
		  ]
	"""
	def parse_matches(self, matches):
		print "Getting prices for items.."
		parsed = []
		counter = 1
		for match in matches:
			print "--------Match {0} out of {1}-------".format(counter, len(matches))
			items = []
			status = match[2].find_all("td")[1].get_text()
			date = match[2].find_all("td")[6].get_text().strip()
			print "Match status: {0} | {1}\n".format(date, status.title())
			counter += 1
			itemdivs = None
			if status == "won":
				try:
					itemdivs = match[0].find_all("td")[1].find_all("div", "item") 
				except IndexError: #If match was won but no items were won
					print "Value earned: $0"
					print "-------End of match-------\n"
					continue
			elif status == "lost":
				itemdivs = match[1].find_all("td")[1].find_all("div", "item") 
			else: #closed
				continue
			items = self.parse_values(itemdivs)		
			value = sum(items) if status == "won" else sum(items)*(-1)
			if not value == 0:
				parsed.append([date.encode('ascii'), value])
			print "Value earned: $" + str(value)
			print "-------End of match-------\n"
		return parsed
	def parse_values(self, itemdivs):
		items = []
		threads = []
		for item in itemdivs:
			name = item.find("div", "name").find("b").get_text()
			if name in self.saved_prices:
				print name.replace(u"\u2122", "") + " | $" + str(self.saved_prices[name]) + " [Saved Price]"
				items.append(self.saved_prices[name])
			else:
				t = Thread(target=self.get_price, args=(name, items,))
				t.start()
				threads.append(t)
				time.sleep(1.5)
			[x.join() for x in threads]	
		return items
	#Find out what the newest, not loaded match is and from that get all the not loaded matches
	def parse_new_matches(self, scraped_matches):
		new_matches = []
		if len(self.saved_matches) > 0:
			for match in scraped_matches[::-1]:
				date = match[2].find_all("td")[6].get_text().strip()
				if self.saved_matches[len(self.saved_matches)-1][0] == date: #Compare dates to find differences
					return new_matches[::-1]
				else:
					new_matches.append(match)
		else:
			return scraped_matches
		return new_matches[::-1]
	def decompress_data(self, data):
		gzipper = gzip.GzipFile(fileobj=StringIO.StringIO(data))
		return gzipper.read()


