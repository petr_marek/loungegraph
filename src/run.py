import graph
import writer
import scraper
import converter as conv
import argparse
import sys
import time

#Write saved item prices to items.txt
def write_items(items, path):
	writer.write_items(items, path)
#Read saved prices from items.txt
def read_prices(path):
	prices = {}
	if not args.r:
		prices = writer.read_items(path)
	return prices
#Write saved matches to matches.txt 
def write_matches(matches, path):
	writer.write_matches(matches, path)
#Read saved matches from matches.txt
def read_matches(path):
	matches = []
	if not args.r:
		matches = writer.read_matches(path)
	return matches
#Read cookie from cookie.txt
def read_cookie(path):
	return writer.read_cookie(path)
def create_graph(data, value):
	if not args.s:
		print "Creating graph.."
		grapher = graph.Graph(data, value)
		grapher.create_graph()	
def setup_args():
	arg_parser = argparse.ArgumentParser()
	arg_parser.add_argument("-c", help="Directory where to look for cookie.txt (Full path)", type=str)
	arg_parser.add_argument("-m", help="Directory where matches.txt will be saved (Full path).", type=str)
	arg_parser.add_argument("-i", help="Directory where items.txt will be saved (Full path).", type=str)
	arg_parser.add_argument("-r", help="Reset all saved data.", action="store_true")
	arg_parser.add_argument("-s", help="Don't open graph when done (Only updates saved data)", action="store_true")
	arg_parser.add_argument("-f", help="Start the program without waiting for user input.", action="store_true")
	arg_parser.add_argument("--dir", help="Directory where all files will be saved and read from (Full path). Discards -c -i -m arguments.", type=str)
	arg_parser.add_argument("--html", help="Html file where to read matches from.", type=str)
	return arg_parser.parse_args()
def print_args():
	if not args.dir:
		if args.c:
			print "-c Argument active, looking for cookie.txt in dir: " + args.c
		if args.m:
			print "-m Argument active, reading and writing matches.txt in dir: " + args.m
		if args.i:
			print "-i Argument active, reading and writing items.txt in dir: " + args.i
	else:
		print "--dir Argument active, all file writing and reading will happen in dir: " + args.dir
	if args.r:
		print "-r Argument active, resetting matches.txt and items.txt."
	if args.s:
		print "-s Argument active, not opening graph after scraping data."
	if args.f:
		print "-f Argument active, user input not required."
	if args.html:
		print "--html Argument active, reading matches from: " + args.html

def print_header():	
	header ="""
           ##########################################################
          ##                                                        ##
          ##  ##                                                    ##
          ##  ##                                                    ##
          ##  ##        ####   ##    ## ## ####    ####    ######   ##
          ##  ##      ##    ## ##    ## ###   ## ##    ## ##    ##  ##
          ##  ##      ##    ## ##    ## ##    ## ##    ## ########  ##
          ##  ##      ##    ## ##    ## ##    ## ##    ## ##        ##
          ##  #######   ####     ####   ##    ##   ######  ######   ##
          ##                                           ##           ##
          ##                                     ##    ##           ##
          ##               Made by Rivenation!    ######            ##
          ##     #####                                              ##
          ##   ##     ##                            ##              ##
          ##  ##      ## ## ####   ######   ######  ##              ##
          ##  ##         ####  ## #     ## ##    ## ## ####         ##
          ##  ##  ###### ##         ###### ##    ## ###   ##        ##
          ##  ##      ## ##       ##    ## #######  ##    ##        ##
          ##   ##    ##  ##      ##    ### ##       ##    ##        ##
          ##    #####    ##       ##### ## ##       ##    ##        ##
          ##                               ##                       ##
          ##                                                        ##
           ##########################################################
-------------------------------------------------------------------------------
- Use the --help launch argument for a full list of the arguments.
- Place your cookie to the cookie.txt file in your directory before using this!
- Consult the cookie_guide.html file for help on finding your cookie.
- First run takes some time since you don't have any prices saved yet!
- Results may be slightly different each run depending on market prices.
- Feel free to contact me on:
	- Bitbucket: https://bitbucket.org/Rivenation
	- Reddit: http://www.reddit.com/user/g3kzor
	- Email: vitunsc2@gmail.com
-------------------------------------------------------------------------------"""
	print header
def quit_program():
	raw_input("Press enter to quit...")
	sys.exit()

args = setup_args()
print_header()
print_args()
#Paths to directories
cookie_path = ""
matches_path = ""
items_path = ""
if args.dir:
	cookie_path = args.dir
	matches_path = args.dir
	items_path = args.dir
else:
	cookie_path = args.c if not args.c == None else ""
	matches_path = args.m if not args.m == None else ""
	items_path = args.i if not args.i == None else ""
paths = [cookie_path, matches_path, items_path]
fixed_paths = []
#Append forward slash to end of the path if necessary
for path in paths:
	new_path = path
	if "\\" in path:
		print "Invalid path (Use forward slashes instead)"
		quit_program()
	if len(path) > 1 and not path[len(path)-1] == "/": 
		new_path += "/"
	fixed_paths.append(new_path)
cookie = read_cookie(fixed_paths[0])
saved_items = read_prices(fixed_paths[1])
saved_matches = read_matches(fixed_paths[2])
print "Saved items: " + str(len(saved_items))
print "Saved matches: " + str(len(saved_matches))
if cookie == None:
	print "Couldn't find your cookie (Check your cookie.txt file)"
	quit_program()

print "Press enter when you're ready to start."
#Wait for user to press enter if no -f argument
if not args.f:
	raw_input()
converter = conv.Converter()
scrape = scraper.MatchHistory(cookie, saved_items, saved_matches, converter)
matches = [[]]
try:
	if args.html:
		matches = scrape.get_matches(args.html)
	else:
		matches = scrape.get_matches()
except scraper.InvalidCookie as e:
	print "--------------------------------------"
	print e
	print "--------------------------------------"
	quit_program()
value_scraper = scraper.InventoryValue(cookie)
value = value_scraper.get_inventory_value()
print "Inventory value: " + str(value)
write_matches(matches, fixed_paths[2])
print "Writing item prices to file...."
write_items(scrape.saved_prices, fixed_paths[1])

create_graph(matches, value)