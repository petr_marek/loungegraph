class Converter(object):
	#1st input to second for ex. 1st one is 1 dollar -> euro
	#Last updated 23.5.2014
	currency_rates = { 
		"dollar": {
			"euro"  : 0.7338,
			"pound" : 0.5945,
			"ruble" : 34.1495,
			"brl"   : 2.2156
		},
		"euro": {
			"dollar": 1.3627,
			"pound" : 0.8101,
			"ruble" : 46.5355,
			"brl"   : 3.0192
		},
		"pound": {
			"dollar": 1.6822,
			"euro"  : 1.2345,
			"ruble" : 57.4463,
			"brl"   : 3.7271
		},
		"ruble": {
			"dollar": 0.0293,
			"euro"  : 0.0215,
			"pound" : 0.0174,
			"brl"   : 0.0649
		},
		"brl": {
			"dollar": 0.4513,
			"euro"  : 0.3312,
			"pound" : 0.2683,
			"ruble" : 15.4132
		}
	}
	def convert_price(self, price, input_currency, output_currency):
		return price * self.currency_rates[input_currency][output_currency]
